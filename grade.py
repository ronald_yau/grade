#!/usr/bin/env python
# -*- coding: utf-8 -*-

import string
import re
import enchant
import editdistance

def grade(correct_answer, student_answer):
	correct_answer = correct_answer.lower()
	student_answer = student_answer.lower()
	return check_student_answer(correct_answer, student_answer)

def check_student_answer(correct_answer, student_answer):
	stripped_correct_answer = correct_answer.strip(string.punctuation)
	stripped_student_answer = student_answer.strip(string.punctuation)

	if(stripped_correct_answer == stripped_student_answer):
		return (True, None, [])
	else:
		correct_answer_words = stripped_correct_answer.split()
		student_answer_words = stripped_student_answer.split()

		total_correct_words = len(correct_answer_words)
		total_student_words = len(student_answer_words)

		# Figure out if there's a typo
		if(total_correct_words == total_student_words):
			return has_typo_or_wrong_word(correct_answer, correct_answer_words, student_answer, student_answer_words)
		# Figure out if word is missing
		elif((total_correct_words - total_student_words) == 1 ):
			highlight = has_word_missing(correct_answer, correct_answer_words, student_answer, student_answer_words)
			if(len(highlight) == 0):
				return (False, None, [])
			else:
				return (False, "missing", highlight)
		else:
			return (False, None, [])

def has_typo_or_wrong_word(correct_answer, correct_answer_words, student_answer, student_answer_words):
	current_correct_word_location = (0,0)
	current_student_word_location = (0,0)

	has_typo = False
	has_wrong_word = False		
	highlights = []
	for i in range(len(correct_answer_words)):
		current_correct_word_location = find_word_location(correct_answer, correct_answer_words[i], current_correct_word_location[1])
		current_student_word_location = find_word_location(student_answer, student_answer_words[i], current_student_word_location[1])

		blame = is_student_word_correct(correct_answer_words[i], student_answer_words[i])
		if(blame == 'typo'):
			has_typo = True
			highlights.append((current_correct_word_location, current_student_word_location))
		elif(blame == 'wrong_word'):
			if(has_wrong_word):
				return (False, None, [])
			else:
				has_wrong_word = True
				highlights.append((current_correct_word_location, current_student_word_location))
	if(has_wrong_word):
		return (False, 'wrong_word', highlights)
	elif(has_typo):
		return (True, 'typo', highlights)

#look for the missing word
#check if there are other errors
def has_word_missing(correct_answer, correct_answer_words, student_answer, student_answer_words):
	current_correct_word_location = (0,0)
	current_student_word_location = (0,0)

	for i in range(len(correct_answer_words)):
		current_correct_word_location = find_word_location(correct_answer, correct_answer_words[i], current_correct_word_location[1])
		current_student_word_location = find_word_location(student_answer, student_answer_words[i], current_student_word_location[1])
		if(correct_answer_words[i] != student_answer_words[i]):
			missing_word_index = i
			break

	for i in range(missing_word_index + 1, len(correct_answer_words)):
		if(correct_answer_words[i] != student_answer_words[i-1]):
			return []

	index_of_missing_word_in_student_answer = (current_student_word_location[0], current_student_word_location[0])

	return [(current_correct_word_location, index_of_missing_word_in_student_answer)]

def is_student_word_correct(correct_word, student_word):
	edit_distance = editdistance.eval(correct_word, student_word)

	if(edit_distance == 0):
		return None
	elif(edit_distance == 1):
		dictionary = enchant.Dict("en_US")
		if(dictionary.check(student_word)):
			return 'wrong_word'
		return 'typo'
	else:
		return 'wrong_word'

# def get_highlights(correct_answer, correct_word, correct_answer_index, student_answer, student_word, student_answer_index):
# 	correct_word_location = find_word_location(correct_answer, correct_word, correct_answer_index)
# 	student_word_location = find_word_location(student_answer, student_word, student_answer_index)
# 	return ((correct_word_location, student_word_location))

# Assuming that answer is all lowercased
def find_word_location(answer, word, starting_index = 0):
	pattern = r"\b" + word + r"\b"
	m = re.search(pattern, answer[starting_index:])

	if m is not None:
		return (m.start() + starting_index, m.end() + starting_index)
	else:
		return (starting_index, starting_index)

