# README #

The goal of this task is to write a function grade() that grades a student's answer and finds certain common mistakes. In particular, the program should figure out (1) if there is a typo in the student's answer, (2) if a word is missing or (3) if a word is wrong. If the program finds a common mistake it should highlight it.

### Dependencies ###
* [PyEnchant](http://pythonhosted.org/pyenchant/)
* [editdistance](https://pypi.python.org/pypi/editdistance)

### Unit Tests ###
Examples provided in the specification were unit tested under gradetest.py