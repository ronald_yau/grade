#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from grade import grade

class TestGradeMethod(unittest.TestCase):

	def test_examples(self):
		self.assertEqual(grade("house", "house"), (True, None, []))

	def test_ignore_punctuation(self):
		self.assertEqual(grade("house", "house."), (True, None, []))
		self.assertEqual(grade("A house", "a house."), (True, None, []))

	def test_typo_wrong_word_detection(self):

		self.assertEqual(grade("house", "hhouse"), (True, "typo", [((0,5), (0,6))]))
		self.assertEqual(grade("The man eats the cheese.", "Thhe maan eatss thhe chheese"), 
			(True, "typo", [((0, 3), (0, 4)), ((4, 7), (5, 9)), ((8, 12), (10, 15)), ((13, 16), (16, 20)), ((17, 23), (21, 28))]))
		# currently assuming that all blamed words will be listd
		self.assertEqual(grade("The man eats the cheese.", "Thhe maan eatss thhhe chheese"),
			(False, "wrong_word", [((0, 3), (0, 4)), ((4, 7), (5, 9)), ((8, 12), (10, 15)), ((13, 16), (16, 21)), ((17, 23), (22, 29))]))
		self.assertEqual(grade("house", "housed"), (False, "wrong_word", [((0, 5), (0, 6))]))

	def test_missing_word(self):
		self.assertEqual(grade("This is my house.", "This my house!"), (False, "missing", [((5,7), (5,5))]))
		self.assertEqual(grade("This is my house.", "This house!"), (False, None, []))

	def test_wrong_word_detection(self):
		self.assertEqual(grade("This is my house.", "This is mi hhouse"), (False, "wrong_word", [((8,10), (8,10)), ((11, 16),(11,17))]))
		self.assertEqual(grade("This is my house.", "This is your house!"), (False, "wrong_word", [((8,10), (8,12))]))
		self.assertEqual(grade("That is my house.", "This is your house!"), (False, None, []))

	def test_unicode_support(self):
		self.assertEqual(grade(u"über is not an English word", u"über is an English word"),
			(False, "missing", [((8,11), (8,8))]))

	def test_extra_words(self):
		self.assertEqual(grade("house", "house house"), (False, None, []))

if __name__ == "__main__":
	unittest.main()